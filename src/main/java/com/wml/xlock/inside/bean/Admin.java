package com.wml.xlock.inside.bean;

import javax.persistence.*;
import java.io.Serializable;

/**
* @Author: 7221
*
* @title: 管理员类
*/
@Entity
@Table(name = "sys_admin")
public class Admin implements Serializable {
    /**
    *
    * @desc: 主键id
    */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "admin_id")
    private Integer id;

    /**
    *
    * @desc:  用户名
    */
    @Column(name = "admin_name")
    private String name;

    /**
    *
    * @desc:  密码
    */
    @Column(name = "admin_password")
    private String password;

    /**
    *
    * @desc:  邮箱
    */
    @Column(name = "admin_mail")
    private String mail;

    /**
    *
    * @desc: 手机号
    */
    @Column(name = "admin_phone")
    private String phone;

    public Admin(String name, String password, String mail, String phone) {
        this.name = name;
        this.password = password;
        this.mail = mail;
        this.phone = phone;
    }

    public Admin() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", mail='" + mail + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
