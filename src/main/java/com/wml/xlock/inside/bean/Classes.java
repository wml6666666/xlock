package com.wml.xlock.inside.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import javax.persistence.*;
import java.io.Serializable;

/**
* @Author: 7221
*
* @title: 班级实体类
*/
@TableName("t_class")
public class Classes implements Serializable {

    /**
    *
    * @desc: 主键
    */
    @TableId(value = "class_id",type = IdType.AUTO)
    private  Integer id;

    /**
    *
    * @desc: 班级名称
    */
    @TableField("class_name")
    private  String name;

    public Classes( String name) {
        this.name = name;
    }

    public Classes() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Class{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
