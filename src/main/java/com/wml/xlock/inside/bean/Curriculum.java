package com.wml.xlock.inside.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import org.apache.ibatis.annotations.Result;

import javax.persistence.*;

/**
 * @Author: 7221
 *
 * @title: 课程
 */
@Table(name = "t_curriculum")
public class Curriculum {
    /**
    *
    * @desc: 主键
    */
    @TableId(value = "curriculum_id",type = IdType.AUTO)
    private  Integer id;

    /**
    *
    * @desc: 课程名
    */
    @TableField("curriculum_name")
    private  String name;

    /**
    *
    * @desc:  说明
    */
    @TableField("curriculum_explain")
    private  String explain;



    public Curriculum(String name, String explain) {
        this.name = name;
        this.explain = explain;
    }

    public Curriculum() {
    }

    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExplain() {
        return explain;
    }

    public void setExplain(String explain) {
        this.explain = explain;
    }

    @Override
    public String toString() {
        return "Curriculum{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", explain='" + explain + '\'' +
                '}';
    }
}
