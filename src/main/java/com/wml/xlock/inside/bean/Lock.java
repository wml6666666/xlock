package com.wml.xlock.inside.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import java.util.Date;
import java.util.List;

/**
 * @Author 7221
 * @Description //TODO  锁
 * @Date 15:16 2019/6/20
 **/
public class Lock {
    private  Integer id;
    @TableField(value ="lock_id")
    private  Integer lock_id;
    @TableField(value = "lock_name")
    private String  name;
    @TableField(value = "lock_imei")
    private String imei ;//imei号
    @TableField(value = "lock_createTime")
    private Date createTime;
    @TableField(value = "lock_updateTime")
    private Date updateTime;
    @TableField(value = "lock_status")
    private Integer status; //状态0：正常；1：不正常；2：未激活；3：离线
    @TableField(value = "lock_buttPlatformType")
    private Integer bpfType ;
    @TableField(value = "lock_model")
    private String model;   //设备型号
    @TableField(value = "lock_batteryLevel")
    private Integer batteryLevel; //电池电量（0 ~ 100）
    @TableField(value = "lock_signalLevel")
    private Integer signalLevel; //	信号强度（0-5
    @TableField(value = "lock_deviceId")
    private Long deviceId;
    @TableField(value = "lock_deviceType")
    private int deviceType;
    @TableField(value = "lock_remark")
    private String remark; //说明
    @TableField(value = "lock_roomId")
    private Integer room_id; //外键.
    @TableField(exist = false)
    private List<Lockpwd> lockpwdList;

    public Lock(Integer lock_id, String name, String imei, Date createTime, Date updateTime, Integer status, Integer bpfType, String model, Integer batteryLevel, Integer signalLevel, Long deviceId, int deviceType, String remark, Integer room_id) {
        this.lock_id = lock_id;
        this.name = name;
        this.imei = imei;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.status = status;
        this.bpfType = bpfType;
        this.model = model;
        this.batteryLevel = batteryLevel;
        this.signalLevel = signalLevel;
        this.deviceId = deviceId;
        this.deviceType = deviceType;
        this.remark = remark;
        this.room_id = room_id;
    }

    public Lock() {
    }

    public Integer getLock_id() {
        return lock_id;
    }

    public void setLock_id(Integer lock_id) {
        this.lock_id = lock_id;
    }

    public List<Lockpwd> getLockpwdList() {
        return lockpwdList;
    }

    public void setLockpwdList(List<Lockpwd> lockpwdList) {
        this.lockpwdList = lockpwdList;
    }

    public int getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(int deviceType) {
        this.deviceType = deviceType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getBpfType() {
        return bpfType;
    }

    public void setBpfType(Integer bpfType) {
        this.bpfType = bpfType;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getBatteryLevel() {
        return batteryLevel;
    }

    public void setBatteryLevel(Integer batteryLevel) {
        this.batteryLevel = batteryLevel;
    }

    public Integer getSignalLevel() {
        return signalLevel;
    }

    public void setSignalLevel(Integer signalLevel) {
        this.signalLevel = signalLevel;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getRoom_id() {
        return room_id;
    }

    public void setRoom_id(Integer room_id) {
        this.room_id = room_id;
    }

    @Override
    public String toString() {
        return "Lock{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", imei='" + imei + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", status=" + status +
                ", bpfType=" + bpfType +
                ", model='" + model + '\'' +
                ", batteryLevel=" + batteryLevel +
                ", signalLevel=" + signalLevel +
                ", deviceId=" + deviceId +
                ", deviceType=" + deviceType +
                ", remark='" + remark + '\'' +
                ", room_id=" + room_id +
                '}';
    }
}
