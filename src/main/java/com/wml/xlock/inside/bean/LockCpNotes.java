package com.wml.xlock.inside.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import java.util.Date;

/**
 * @Author 7221
 * @Description //TODO  报警
 * @Date 17:47 2019/6/20
 **/

public class LockCpNotes {
    @TableId(value = "LockCpNotes_id",type = IdType.AUTO)
    private  Integer id;
    @TableField(value = "lockCpNotes_imei")
    private  String imei;
    @TableField(value = "lockCpNotes_createTime")
    private Date createTime ;
    @TableField(value = "lockCpNotes_type")
    private  int type; //1防拆报警 2密码连错报警 3低电量报警 4劫持报警 5假锁报警

    public LockCpNotes(String imei, Date createTime, int type) {
        this.imei = imei;
        this.createTime = createTime;
        this.type = type;
    }

    public LockCpNotes() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "LockCpNotes{" +
                "id=" + id +
                ", imei='" + imei + '\'' +
                ", createTime=" + createTime +
                ", type=" + type +
                '}';
    }
}
