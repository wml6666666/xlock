package com.wml.xlock.inside.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import java.util.Date;

/**
 * @Author 7221
 * @Description //TODO  锁得开启记录
 * @Date 18:08 2019/6/20
 **/
public class LockOpenNotes {
    @TableId(value = "lockOpNos_id",type = IdType.AUTO)
    private  Integer id;
    @TableField(value = "lockOpNos_imei")
    private  String imei;
    @TableField(value = "lockOpNos_createTime")
    private Date createTime;
    @TableField(value = "lockOpNos_lockpwdId")
    private Integer lockpwdId;

    public LockOpenNotes(String imei, Date createTime, Integer lockpwdId) {
        this.imei = imei;
        this.createTime = createTime;
        this.lockpwdId = lockpwdId;
    }

    public LockOpenNotes() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getLockpwdId() {
        return lockpwdId;
    }

    public void setLockpwdId(Integer lockpwdId) {
        this.lockpwdId = lockpwdId;
    }

    @Override
    public String toString() {
        return "LockOpenNotesService{" +
                "id=" + id +
                ", imei=" + imei +
                ", createTime=" + createTime +
                ", lockpwdId=" + lockpwdId +
                '}';
    }
}
