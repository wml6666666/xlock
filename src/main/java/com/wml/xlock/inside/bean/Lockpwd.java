package com.wml.xlock.inside.bean;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import java.util.Date;
/**
 * @Author 7221
 * @Description //TODO 锁密码
 * @Date 17:47 2019/6/20
 **/
public class Lockpwd {
    private Integer id ;
    @TableField(value = "lockpwd_id")
    private Integer pwd_id;
    @TableField(value = "lockpwd_lockId")
    private Integer lock_id;
    @TableField(value = "lockpwd_devicePwdId")
    private Long devicePwdId;
    @TableField(value = "lockpwd_lockDeviceId")
    private Long lockDeviceId;
    @TableField(value = "lockpwd_name")
    private String name; //密码名字
    @TableField(value = "lockpwd_content")
    private String content; //密码内容
    @TableField(value = "lockpwd_type")
    private Integer type; //1:指纹；2:密码;3:卡;4:临时密码；
    @TableField(value = "lockpwd_startTime")
    private Date startTime; //密码开始事件
    @TableField(value = "lockpwd_endTime")
    private  Date endTime; //密码结束时间
    @TableField(value = "lockpwd_createTime")
    private Date createTime; //密码开始事件
    @TableField(value = "lockpwd_updateTime")
    private  Date updateTime; //密码结束时间
    @TableField(value = "lockpwd_status")
    private  Integer status; //密码状态 0正常
    @TableField(value = "lockpwd_auth")
    private  Integer auth; //密码权限：1:管理员 2:普通用户 3:劫持用户
    @TableField(value = "lockpwd_unlockTimes")
    private  Integer unlockTimes; // 限制解锁次数

    public Lockpwd(Integer pwd_id, Integer lock_id, Long devicePwdId, Long lockDeviceId, String name, String content, Integer type, Date startTime, Date endTime, Date createTime, Date updateTime, Integer status, Integer auth, Integer unlockTimes) {
        this.pwd_id = pwd_id;
        this.lock_id = lock_id;
        this.devicePwdId = devicePwdId;
        this.lockDeviceId = lockDeviceId;
        this.name = name;
        this.content = content;
        this.type = type;
        this.startTime = startTime;
        this.endTime = endTime;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.status = status;
        this.auth = auth;
        this.unlockTimes = unlockTimes;
    }

    public Lockpwd() {
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLock_id() {
        return lock_id;
    }

    public void setLock_id(Integer lock_id) {
        this.lock_id = lock_id;
    }

    public Long getDevicePwdId() {
        return devicePwdId;
    }

    public void setDevicePwdId(Long devicePwdId) {
        this.devicePwdId = devicePwdId;
    }

    public Long getLockDeviceId() {
        return lockDeviceId;
    }

    public void setLockDeviceId(Long lockDeviceId) {
        this.lockDeviceId = lockDeviceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getPwd_id() {
        return pwd_id;
    }

    public void setPwd_id(Integer pwd_id) {
        this.pwd_id = pwd_id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getAuth() {
        return auth;
    }

    public void setAuth(Integer auth) {
        this.auth = auth;
    }

    public Integer getUnlockTimes() {
        return unlockTimes;
    }

    public void setUnlockTimes(Integer unlockTimes) {
        this.unlockTimes = unlockTimes;
    }

    @Override
    public String toString() {
        return "Lockpwd{" +
                "id=" + id +
                ", lock_id=" + lock_id +
                ", devicePwdId=" + devicePwdId +
                ", lockDeviceId=" + lockDeviceId +
                ", name='" + name + '\'' +
                ", content='" + content + '\'' +
                ", type=" + type +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", status=" + status +
                ", auth=" + auth +
                ", unlockTimes=" + unlockTimes +
                '}';
    }
}
