package com.wml.xlock.inside.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import javax.persistence.*;

/**
* @Author: 7221
*
* @title: 上课点---教室
*/
@Entity
@Table(name = "t_room")
public class Room {

    /**
    *
    * @desc: 主键
    */
    @TableId(value = "room_id",type = IdType.AUTO)
    private Integer id;

    /**
    *
    * @desc: 楼名
    */
    @TableField("room_name")
    private String name;

    /**
    *
    * @desc: 楼层
    */
    @TableField("room_floor")
    private String floor;

    /**
    *
    * @desc: 房间号
    */
    @TableField("room_num")
    private String num;

    public Room(String name, String floor, String num) {
        this.name = name;
        this.floor = floor;
        this.num = num;
    }

    public Room() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    @Override
    public String toString() {
        return "Room{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", floor='" + floor + '\'' +
                ", num='" + num + '\'' +
                '}';
    }
}
