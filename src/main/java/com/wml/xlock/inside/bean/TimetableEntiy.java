package com.wml.xlock.inside.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

/**
 * @Author 7221
 * @Description //TODO 实体类
 * @Date 21:22 2019/6/5
 **/
@TableName("t_timetable")
public class TimetableEntiy {
    @TableId(value = "timetable_id",type = IdType.AUTO)
    private  Integer id;
    private  Integer user_id;
    private  Integer curriculum_id;
    private  Date timetable_time;
    private  Integer room_id;
    private  Integer class_id;
    private  Integer timetable_weekday;

    public TimetableEntiy() {
    }

    public TimetableEntiy(Integer user_id, Integer curriculum_id, Date timetable_time, Integer room_id, Integer class_id, Integer timetable_weekday) {
        this.user_id = user_id;
        this.curriculum_id = curriculum_id;
        this.timetable_time = timetable_time;
        this.room_id = room_id;
        this.class_id = class_id;
        this.timetable_weekday = timetable_weekday;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getCurriculum_id() {
        return curriculum_id;
    }

    public void setCurriculum_id(Integer curriculum_id) {
        this.curriculum_id = curriculum_id;
    }

    public Date getTimetable_time() {
        return timetable_time;
    }

    public void setTimetable_time(Date timetable_time) {
        this.timetable_time = timetable_time;
    }

    public Integer getRoom_id() {
        return room_id;
    }

    public void setRoom_id(Integer room_id) {
        this.room_id = room_id;
    }

    public Integer getClass_id() {
        return class_id;
    }

    public void setClass_id(Integer class_id) {
        this.class_id = class_id;
    }

    public Integer getTimetable_weekday() {
        return timetable_weekday;
    }

    public void setTimetable_weekday(Integer timetable_weekday) {
        this.timetable_weekday = timetable_weekday;
    }

    @Override
    public String toString() {
        return "TimetableEntiy{" +
                "id=" + id +
                ", user_id=" + user_id +
                ", curriculum_id=" + curriculum_id +
                ", timetable_time=" + timetable_time +
                ", room_id=" + room_id +
                ", class_id=" + class_id +
                ", timetable_weekday=" + timetable_weekday +
                '}';
    }
}
