package com.wml.xlock.inside.bean;


import java.util.Date;

/**
* @Author: 7221
* @desc ： 中间类
* @title: 课表
*/
public class TimetableMiddble {

    /**
    *
    * @desc:  主键
    */
    private  Integer id;

    /**
    *
    * @desc:  上课时间
    */
    private Date time;

    /**
    *
    * @desc:  上课班级
    */
    private Classes aclass;

    /**
    *
    * @desc: 教室
    */
    private Room room;

    /**
    *
    * @desc: 课程名
    */
    private Curriculum curriculum;

    /**
    *
    * @desc:  任课教师
    */
    private User user;

    public Integer getWeekday() {
        return weekday;
    }

    public void setWeekday(Integer weekday) {
        this.weekday = weekday;
    }

    /**
     * @Author 7221
     * @Description //TODO  1-8 周次
     * @Date 20:56 2019/6/1
     **/
    private Integer weekday;

    public TimetableMiddble(Date time, Classes aclass, Room room, Curriculum curriculum, User user, Integer weekday) {
        this.time = time;
        this.aclass = aclass;
        this.room = room;
        this.curriculum = curriculum;
        this.user = user;
        this.weekday = weekday;
    }

    public TimetableMiddble() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Classes getAclass() {
        return aclass;
    }

    public void setAclass(Classes aclass) {
        this.aclass = aclass;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Curriculum getCurriculum() {
        return curriculum;
    }

    public void setCurriculum(Curriculum curriculum) {
        this.curriculum = curriculum;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "TimetableMiddble{" +
                "id=" + id +
                ", time=" + time +
                ", aclass=" + aclass +
                ", room=" + room +
                ", curriculum=" + curriculum +
                ", user=" + user +
                ", weekday=" + weekday +
                '}';
    }
}
