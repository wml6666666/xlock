package com.wml.xlock.inside.bean;


import javax.persistence.*;
import java.util.Date;

/**
* @Author: 7221
*
* @title:   教师
*/
@Table(name = "t_user")
public class User {

    /**
    *
    * @desc:  主键
    */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Integer id;

    /**
    *
    * @desc: 手机号
    */
    @Column(name = "user_phone")
    private String phone;

    /**
    *
    * @desc:  姓名
    */
    @Column(name = "user_name")
    private String name;


    /**
    *
    * @desc: 微信号
    */
    @Column(name = "user_wxid")
    private String wxid;

    /**
    *
    * @desc: 性别
    */
    @Column(name = "user_sex")
    private Character sex;

    public User(String phone, String name, String wxid, Character sex) {
        this.phone = phone;
        this.name = name;
        this.wxid = wxid;
        this.sex = sex;
    }

    public User() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWxid() {
        return wxid;
    }

    public void setWxid(String wxid) {
        this.wxid = wxid;
    }

    public Character getSex() {
        return sex;
    }

    public void setSex(Character sex) {
        this.sex = sex;
    }
}
