package com.wml.xlock.inside.controller;

import com.github.pagehelper.PageInfo;
import com.wml.xlock.inside.bean.Admin;
import com.wml.xlock.inside.service.entity.AdminService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import pers.vankid.common.model.base.CommonResponse;
import pers.vankid.common.utils.jdk.MD5Util;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("admin")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @PostMapping("add")
    public CommonResponse newAdmin(@RequestBody Admin admin){
        admin.setPassword(MD5Util.ecodeByMD5(admin.getPassword()));
        Boolean b = adminService.newAdmin(admin);
        return CommonResponse.buildUpdateSuccessResponse(b ? "添加成功" : "添加失败", b);
    }

    @PutMapping("updateAdmin")
    public CommonResponse updateAdmin(@RequestBody Admin admin){
        Boolean b = adminService.updateAdmin(admin);
        return CommonResponse.buildUpdateSuccessResponse(b ? "修改成功" : "修改失败", b);
    }

    @GetMapping("{id}")
    public Admin getAdminById(@PathVariable Integer id){
        return adminService.getAdminById(id);
    }

    @PostMapping("list/{num}")
    public PageInfo<List<Admin>> getAdminList(@RequestBody Admin admin, @PathVariable int num,@Value("${page.pageSize}") int size){
        return adminService.getAdminList(admin,num,size);
    }

    @PostMapping("login")
    public Object adminLogin(@RequestBody Admin admin, HttpSession session){
        admin.setPassword(MD5Util.ecodeByMD5(admin.getPassword()));
        Admin admin1 = adminService.AdminLogin(admin);
        Map map=new HashMap(1);
        map.put("result",admin1);
        if(admin1!=null){
            session.setAttribute("login",admin1);
        }
        return map;
    }

}
