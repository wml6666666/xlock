package com.wml.xlock.inside.controller;

import com.github.pagehelper.PageInfo;
import com.wml.xlock.inside.bean.Classes;
import com.wml.xlock.inside.service.entity.ClassesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import pers.vankid.common.model.base.CommonResponse;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("classes")
public class ClassesController {

    @Autowired
    private ClassesService classesService;

    @PostMapping("addClass")
    public Object newClasses(@RequestBody Classes classes){
        Boolean b = classesService.newClasses(classes);
        return CommonResponse.buildUpdateSuccessResponse(b ? "添加成功" : "添加失败", b);
    }

    @GetMapping("{id}")
    public Classes getClassesById(@PathVariable Integer id){
        return classesService.getClassesById(id);
    }

    @PostMapping("list/{num}")
    public PageInfo<List<Classes>> getClassesList(@RequestBody(required = false) Classes classes, @PathVariable int num,@Value("${page.pageSize}") int size){
        return classesService.getClassesList(classes,num,size);
    }
    @PostMapping("delClassesByIds")
    public CommonResponse delClassesByIds(@RequestBody List<Integer> list){
        Boolean b = classesService.delClassesByIds(list);
        return CommonResponse.buildDelSuccessResponse(b ? "删除成功" : "删除失败", b);
    }

}
