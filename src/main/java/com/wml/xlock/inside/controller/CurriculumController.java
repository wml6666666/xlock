package com.wml.xlock.inside.controller;

import com.github.pagehelper.PageInfo;
import com.wml.xlock.inside.bean.Curriculum;
import com.wml.xlock.inside.service.entity.CurriculumService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import pers.vankid.common.model.base.CommonResponse;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("curriculum")
public class CurriculumController {

    @Autowired
    private CurriculumService curriculumService;

    @PostMapping("addCurriculum")
    public CommonResponse newCurriculum(@RequestBody Curriculum curriculum){
        Boolean b = curriculumService.newCurriculum(curriculum);
        return CommonResponse.buildUpdateSuccessResponse(b ? "添加成功" : "添加失败", b);
    }

    @PutMapping("updateCurriculum")
    public CommonResponse updateCurriculum(@RequestBody Curriculum curriculum){
        Boolean b = curriculumService.updateCurriculum(curriculum);
        return CommonResponse.buildUpdateSuccessResponse(b ? "修改成功" : "修改失败", b);
    }

    @GetMapping("{id}")
    public Curriculum getCurriculumById(@PathVariable Integer id){
        return curriculumService.getCurriculumById(id);
    }

    @PostMapping("list/{num}")
    public PageInfo<List<Curriculum>> getCurriculumList(@RequestBody(required = false) Curriculum curriculum, @PathVariable int num,@Value("${page.pageSize}") int size){
        return curriculumService.getCurriculumList(curriculum,num,size);
    }

    @DeleteMapping ("delCurriculumByIds")
    public CommonResponse delClassesByIds(@RequestBody List<Integer> list){
        Boolean b = curriculumService.delCurriculumsByIds(list);
        return CommonResponse.buildDelSuccessResponse(b ? "删除成功" : "删除失败", b);
    }

}
