package com.wml.xlock.inside.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

//@ControllerAdvice
public class ExceptionController {

    @ResponseBody
    @ExceptionHandler(Exception.class)
    public Map handleException(Exception e){
        Map map=new HashMap();
        map.put("status",500);
        map.put("result","error");
        map.put("message",e.getMessage());
        return  map;
    }
}
