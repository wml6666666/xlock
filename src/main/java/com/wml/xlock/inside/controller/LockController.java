package com.wml.xlock.inside.controller;

import com.github.pagehelper.PageInfo;
import com.wml.xlock.inside.bean.Lock;
import com.wml.xlock.inside.service.entity.LockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController("inLockController")
@RequestMapping("/inside/lock")
public class LockController {
    @Autowired
    LockService lockService;
    @PostMapping("/bindlockroom")
    public Object bindLock_room(@RequestParam  Integer lock_id, @RequestParam Integer room_id ){
        boolean b = lockService.bindLock_Room(lock_id, room_id);
        return b;
    }
    @PostMapping("/locks")
    public Object getLocksByPage(@RequestParam Integer current,@RequestParam Integer size){
        PageInfo<Lock> locksBypage = lockService.getLocksBypage(current, size);
        return locksBypage;
    }
    @PostMapping("/locksByRoomId")
    public Object getLocksByRoomId(@RequestParam Integer room_id){
        List<Lock> locks = lockService.getLocksByRoomId(room_id);
        return locks;
    }
    @PostMapping("/uplockname")
    public Object uplockname(@RequestParam Integer id,@RequestParam String newname){
        Lock lock=new Lock();
        lock.setId(id);
        lock.setName(newname);
        boolean b = lockService.uplockNameById(lock);
        return b;
    }
}
