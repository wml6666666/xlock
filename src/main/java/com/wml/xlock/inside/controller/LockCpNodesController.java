package com.wml.xlock.inside.controller;

import com.github.pagehelper.PageInfo;
import com.wml.xlock.inside.bean.Lock;
import com.wml.xlock.inside.bean.LockCpNotes;
import com.wml.xlock.inside.service.entity.LockcpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("lockcp")
public class LockCpNodesController {
    @Autowired
    LockcpService lockcpService;
    @PostMapping("/getlockcps")
    public Object getlockcpsBypage(@RequestParam Integer current, @RequestParam Integer size){
        PageInfo<LockCpNotes> lockCpNotesPageInfo = lockcpService.getlockcpBypage(current, size);
        return lockCpNotesPageInfo;
    }
    @PostMapping("/dellockcps")
    public boolean delLockcpByIds(List<Integer> ids){
        boolean b = lockcpService.delLockcpByIds(ids);
        return  b;
    }
}
