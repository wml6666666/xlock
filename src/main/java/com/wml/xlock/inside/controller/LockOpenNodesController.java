package com.wml.xlock.inside.controller;

import com.github.pagehelper.PageInfo;
import com.wml.xlock.inside.bean.LockOpenNotes;
import com.wml.xlock.inside.service.entity.LockOpenNotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("lockop")
public class LockOpenNodesController {
    @Autowired
    LockOpenNotesService lockOpenNotesService;
    @PostMapping("/getlockops")
    public Object getlockopsBypage(@RequestParam Integer current, @RequestParam Integer size){
        PageInfo<LockOpenNotes> lockCpNotesPageInfo = lockOpenNotesService.getOpenNsBypage(current, size);
        return lockCpNotesPageInfo;
    }
    @PostMapping("/dellockops")
    public boolean delLockopByIds(List<Integer> ids){
        boolean b = lockOpenNotesService.delOpenNByIds(ids);
        return  b;
    }
}
