package com.wml.xlock.inside.controller;

import com.github.pagehelper.PageInfo;
import com.wml.xlock.inside.bean.Lockpwd;
import com.wml.xlock.inside.service.entity.LockService;
import com.wml.xlock.inside.service.entity.LockpwdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/lockpwd")
public class LockpwdController {

    @Autowired
    LockpwdService lockpwdService;

    /**
     *  分页查询锁的密码列表
     * @param lockId 锁id
     * @param type 密码类型 (1:指纹；2:密码，3、卡、4：临时密码 0 全部）
     * @param current 页数
     * @param size 页数据数
     * @return
     */
    @PostMapping("/getpwdsByLockID")
    public PageInfo<Lockpwd> getlockpwdByLockID(Integer lockId,Integer type,Integer current,Integer size){
        PageInfo<Lockpwd> lockpwdsByLockId = lockpwdService.getLockpwdsByLockId(lockId, type, current, size);
        return  lockpwdsByLockId;
    }
}
