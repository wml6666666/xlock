package com.wml.xlock.inside.controller;

import com.github.pagehelper.PageInfo;
import com.wml.xlock.inside.bean.Room;
import com.wml.xlock.inside.service.entity.RoomService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import pers.vankid.common.model.base.CommonResponse;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("room")
public class RoomController {

    @Autowired
    private RoomService roomService;

    @PostMapping("addRoom")
    public CommonResponse newRoom(@RequestBody Room room){
        Boolean b = roomService.newRoom(room);
        return CommonResponse.buildUpdateSuccessResponse(b ? "添加成功" : "添加失败", b);
    }

    @PutMapping("updateRoom")
    public CommonResponse upRoom(@RequestBody Room room){
        Boolean b = roomService.updateRoom(room);
        return CommonResponse.buildUpdateSuccessResponse(b ? "修改成功" : "修改失败", b);
    }

    @GetMapping("{id}")
    public Room getRoomById(@PathVariable Integer id){
        return roomService.getRoomById(id);
    }

    @PostMapping("list/{num}")
    public PageInfo<List<Room>> getRoomList(@RequestBody(required = false) Room room, @PathVariable int num,@Value("${page.pageSize}") int size){
        return roomService.getRoomList(room,num,size);
    }

    @PostMapping("delRoomByIds")
    public CommonResponse delRoomByIds(@RequestBody List<Integer> list){
        Boolean b = roomService.delClassesByIds(list);
        return CommonResponse.buildDelSuccessResponse(b ? "删除成功" : "删除失败", b);
    }

}
