package com.wml.xlock.inside.controller;

import com.wml.xlock.inside.bean.TimetableEntiy;
import com.wml.xlock.inside.service.entity.TimetableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pers.vankid.common.model.base.CommonResponse;

import java.util.List;

/**
 * @Author 7221
 * @Description //TODO 课程表的crud
 * @Date 16:25 2019/6/3
 **/
@RestController
@RequestMapping("/timetable")
public class TimetableController {
    @Autowired
    TimetableService timetableService;

    /**
     * @Author 7221
     * @Description //TODO 插入课表记录
     * @Date 16:25 2019/6/3
     * @Param TimetableEntiy timetableEntiy
     * @return java.lang.Object
     **/

    @PostMapping("/addTimetable")
    public Object insertTimetable(@RequestBody TimetableEntiy timetableEntiy){
        boolean b= timetableService.insertTimetable(timetableEntiy);
        return  CommonResponse.buildUpdateSuccessResponse(b ? "插入成功" : "插入失败", b);
    }

    /**
     * @Author 7221
     * @Description //TODO  删除
     * @Date 16:26 2019/6/3
     * @Param [id]
     * @return java.lang.Object
     **/
    @DeleteMapping("/delTimetable/{id}")
    public Object deleteTimetableById(@PathVariable Integer id) {
        boolean b= timetableService.deleteTimetableById(id);
        return  CommonResponse.buildUpdateSuccessResponse(b ? "删除成功" : "删除失败", b);
    }

    /**
     * @Author 7221
     * @Description //TODO 批量删除
     * @Date 11:13 2019/6/14
     * @Param [list]
     * @return java.lang.Object
     **/
    @DeleteMapping("/delTimetableByList")
    public Object deleteTimetableByList(@RequestBody List<Integer> list) {
        boolean b= timetableService.deleteTimetableByList(list);
        return  CommonResponse.buildUpdateSuccessResponse(b ? "删除成功" : "删除失败", b);
    }

    /**
     * @Author 7221
     * @Description //TODO 修改数据
     * @Date 16:26 2019/6/3
     * @Param [map]
     * @return java.lang.Object
     **/
    @PutMapping("/upTimetable")
    public CommonResponse upTimetable(@RequestBody TimetableEntiy timetableEntiy) {
        boolean b= timetableService.updateTimetableById(timetableEntiy);
        return  CommonResponse.buildUpdateSuccessResponse(b ? "修改成功" : "修改失败", b);
    }
//    /**
//     * @Author 7221
//     * @Description //TODO 分页
//     * @Date 16:26 2019/6/3
//     * @Param [pagenum]
//     * @return java.lang.Object
//     **/
//    @GetMapping("/findTimetable/{pagenum}")
//    public Object selectPageAll(@PathVariable Integer pagenum) {
//        return timetableService.selectPageAll(pagenum);
//    }

    /**
     * @Author 7221
     * @Description //TODO 根据Entiy参数动态查询
     * @Date 16:27 2019/6/3
     * @Param [map]
     * @return java.lang.Object
     **/
    @PostMapping("/findTimetable/{pagenum}")
    public Object selectPageByEntiy(@RequestBody(required = false)  TimetableEntiy timetableEntiy,@PathVariable Integer pagenum) {
        return timetableService.getTimetableByEntiy(timetableEntiy,pagenum);
    }


}
