package com.wml.xlock.inside.controller;

import com.github.pagehelper.PageInfo;
import com.wml.xlock.inside.bean.User;
import com.wml.xlock.inside.service.entity.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import pers.vankid.common.model.base.CommonResponse;
import pers.vankid.common.utils.jdk.MD5Util;

import javax.servlet.http.HttpSession;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("addUser")
    public CommonResponse newUser(@RequestBody User user){
        Boolean b = userService.newUser(user);
        return CommonResponse.buildUpdateSuccessResponse(b ? "添加成功" : "添加失败", b);
    }

    @PutMapping("updateUser")
    public CommonResponse upUser(@RequestBody User user){
        Boolean b = userService.updateUser(user);
        return CommonResponse.buildUpdateSuccessResponse(b ? "修改成功" : "修改失败", b);
    }

    @GetMapping("{id}")
    public User getUserById(@PathVariable Integer id){
        return userService.getUserById(id);
    }

    @PostMapping("list/{num}")
    public PageInfo<List<User>> getUserList(@RequestBody User user, @PathVariable int num,@Value("${page.pageSize}") int size){
        return userService.getUserList(user,num,size);
    }

}
