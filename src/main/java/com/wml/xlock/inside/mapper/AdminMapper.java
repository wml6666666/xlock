package com.wml.xlock.inside.mapper;

import com.wml.xlock.inside.bean.Admin;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

@CacheConfig(cacheNames = "admin")
public interface AdminMapper extends Mapper<Admin> {
    @Cacheable
   List<Admin> getAdminList(Admin admin);

    @Cacheable
    Admin AdminLogin(Admin admin);

}
