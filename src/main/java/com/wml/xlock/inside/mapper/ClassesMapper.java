package com.wml.xlock.inside.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wml.xlock.inside.bean.Classes;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;


public interface ClassesMapper extends BaseMapper<Classes> {


    List<Classes> getClassesList(Classes classes);
}
