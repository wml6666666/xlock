package com.wml.xlock.inside.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wml.xlock.inside.bean.Curriculum;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface CurriculumMapper extends BaseMapper<Curriculum> {
    List<Curriculum> getCurriculumList(Curriculum curriculum);
    Curriculum getCurriculumById(Integer id);
}
