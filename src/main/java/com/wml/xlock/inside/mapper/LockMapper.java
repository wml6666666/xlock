package com.wml.xlock.inside.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wml.xlock.inside.bean.Lock;

public interface LockMapper extends BaseMapper<Lock> {
}
