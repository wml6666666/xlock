package com.wml.xlock.inside.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wml.xlock.inside.bean.LockOpenNotes;

public interface LockOpenNotesMappper extends BaseMapper<LockOpenNotes> {
}
