package com.wml.xlock.inside.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wml.xlock.inside.bean.LockCpNotes;

public interface LockcpMapper extends BaseMapper<LockCpNotes> {
}
