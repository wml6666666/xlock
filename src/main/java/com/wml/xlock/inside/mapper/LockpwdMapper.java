package com.wml.xlock.inside.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wml.xlock.inside.bean.Lockpwd;

public interface LockpwdMapper extends BaseMapper<Lockpwd> {
}
