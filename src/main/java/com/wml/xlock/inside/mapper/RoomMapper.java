package com.wml.xlock.inside.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wml.xlock.inside.bean.Room;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface RoomMapper extends BaseMapper<Room> {
    List<Room> getRoomList(Room room);
}
