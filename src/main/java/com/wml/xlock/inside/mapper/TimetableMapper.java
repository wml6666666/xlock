package com.wml.xlock.inside.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wml.xlock.inside.bean.TimetableEntiy;
import com.wml.xlock.inside.bean.TimetableMiddble;

import java.util.List;


public interface TimetableMapper extends BaseMapper<TimetableEntiy> {

    List<TimetableMiddble> getTimetableByEntiy(TimetableEntiy timetableEntiy);
}
