package com.wml.xlock.inside.mapper;

import com.wml.xlock.inside.bean.User;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;


public interface UserMapper extends Mapper<User> {
    List<User> getUserList(User user);

    User UserLogin(User user);
}
