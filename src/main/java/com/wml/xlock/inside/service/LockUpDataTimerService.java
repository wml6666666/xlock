package com.wml.xlock.inside.service;

import com.wml.xlock.inside.service.entity.LockService;
import com.wml.xlock.outside.bean.OutRetBen;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class LockUpDataTimerService {
    @Autowired
    com.wml.xlock.outside.service.LockService lockoutService;
    @Autowired
    LockService lockService;
    @Scheduled(cron = "0 0 0 * * ?")
    public  void uplocktb(){
        OutRetBen<List<Map>> listOutRetBen = lockoutService.getallLock();
        List<Map> data=listOutRetBen.getData();
        for (Map map:
             data) {
            Integer lockBatteryLevel=(Integer) map.get("lockBatteryLevel");
            Integer lockSignalLevel=(Integer) map.get("lockSignalLevel");
            Map setmap=new HashMap();
            setmap.put("lock_batteryLevel",lockBatteryLevel);
            setmap.put("lock_signalLevel",lockSignalLevel);
            lockService.syncLockInfoByLockId((Integer) map.get("lockId"), setmap);
        }

    }
}
