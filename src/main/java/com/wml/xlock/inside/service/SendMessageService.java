package com.wml.xlock.inside.service;

 import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
 import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class SendMessageService {

    private final  String code="【南京科技职业学院】";

    @Async
    public void sendMsg(String mobile,String message){
        //发送通知短信
        DefaultProfile profile = DefaultProfile.getProfile("default", "LTAI56rggX65QFVM", "pecXsCyp2ojT5hEzn1Xp9bbBEyraT7");
        IAcsClient client = new DefaultAcsClient(profile);
        //用户登录名称 znms@1849460414114784.onaliyun.com
        //AccessKey ID LTAI56rggX65QFVM
        //AccessKeySecret pecXsCyp2ojT5hEzn1Xp9bbBEyraT7
        CommonRequest request = new CommonRequest();
        //request.setProtocol(ProtocolType.HTTPS);
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");
        request.setVersion("2017-05-25");
        request.setAction("SendSms");
        request.putQueryParameter("PhoneNumbers", mobile);
        request.putQueryParameter("SignName", "南京科技职业学院信工院");
        request.putQueryParameter("TemplateCode", "SMS_167960953");
        request.putQueryParameter("TemplateParam",message);
        try {
            CommonResponse response = client.getCommonResponse(request);
            System.out.printf("test:"+response.getData());
        } catch (ServerException e) {
        } catch (ClientException e) {
        }

    }

}
