package com.wml.xlock.inside.service;

import com.wml.xlock.inside.bean.*;
import com.wml.xlock.inside.service.entity.LockpwdService;
import com.wml.xlock.inside.service.entity.TimetableService;
import com.wml.xlock.outside.bean.OutRetBen;
import com.wml.xlock.outside.service.LockService;
import com.wml.xlock.outside.util.StringDateFormate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class TimetableTimerService {
    @Autowired
    TimetableService timetableService;

    @Autowired
    SendMessageService sendMessageService;

    @Autowired
    LockpwdService lockpwdService;

    @Autowired
    LockService lockService;

    @Autowired
    com.wml.xlock.inside.service.entity.LockService lockinService;

    /**
     * @Author 7221
     * @Description //TODO 定时发短信
     * @Date 14:04 2019/6/3
     * @Param []
     * @return void
     **/
    @Scheduled(cron = "0 20 7 * * 1-5")
    public void ttTimer(){
        send("07:20");
    }

    @Scheduled(cron = "0 20 9 * * 1-5")
    public void ttTimer2(){
        send("09:20");
    }


    @Scheduled(cron = "0 30 12 * * 1-5")
    public void ttTimer3(){
        send("12:30");
    }

    @Scheduled(cron = "0 30 14 * * 1-5")
    public void ttTimer4(){
        send("14:30");
    }
    @Transactional
    private void send(String time){
        Calendar calendar=Calendar.getInstance();
        int i = calendar.get(Calendar.DAY_OF_WEEK);
        TimetableEntiy timetableEntiy=new TimetableEntiy();
        timetableEntiy.setTimetable_weekday(i==1?7:i-1);
        timetableEntiy.setTimetable_time(StringDateFormate.parseDate(time));
        List<TimetableMiddble> timetableMiddbles = timetableService.selectAll(timetableEntiy);
        for (int j = 0; j < timetableMiddbles.size() ; j++) {
            TimetableMiddble timetableMiddble = timetableMiddbles.get(j);
            Room room = timetableMiddble.getRoom();
            String phone= timetableMiddble.getUser().getPhone();
            String name = timetableMiddble.getUser().getName();
            String f_name =room.getName();
            String floor = room.getFloor();
            String num = room.getNum();
            String flname=f_name+floor+num;
            String rdstr = (int)(Math.random()*100000)+"";
            int len=rdstr.length();
            if(len<6){
                for (int k = 0; k <6-len; k++) {
                    rdstr=0+rdstr;
                }
            }
            List<Lock> locks = lockinService.getLocksByRoomId(room.getId());
            for (Lock lock : locks ) {
                Date starttime = calendar.getTime();
                int i1 = calendar.get(Calendar.HOUR_OF_DAY);
                calendar.set(Calendar.HOUR_OF_DAY,i1+3);
                Date endtime = calendar.getTime();
                OutRetBen<Map> setlocktemppwd = lockService.setlocktemppwd(lock.getId(), flname, rdstr, StringDateFormate.formatDate(starttime), StringDateFormate.formatDate(endtime));
                if("success".equals(setlocktemppwd.getMsg())) {
                    Map data = setlocktemppwd.getData();
                    lockpwdService.delLockpwdByType(4);
                    Lockpwd lockpwd=new Lockpwd((Integer) data.get("pwdId"),(Integer) data.get("pwdLockId"),(Long)data.get("pwdDevicePwdId"),(Long)data.get("pwdLockDeviceId"),data.get("pwdName").toString(),data.get("pwdContent").toString(),(Integer) data.get("pwdType"), StringDateFormate.parseDate(data.get("pwdStartTime").toString()),StringDateFormate.parseDate(data.get("pwdEndTime").toString()),StringDateFormate.parseDate(data.get("pwdCreateTime").toString()),StringDateFormate.parseDate(data.get("pwdUpdateTime").toString()),0,2,0);
                    lockpwdService.addLockpwd(lockpwd);
                }

            }
            String  jsons="{name:'"+name+"',floor:'"+flname+"',pass:'"+rdstr+"'}";
            sendMessageService.sendMsg(phone,jsons);
        }
    }


}
