package com.wml.xlock.inside.service.entity;


import com.github.pagehelper.PageInfo;
import com.wml.xlock.inside.bean.Admin;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
* @Author: 7221
*
* @title: 管理员业务层接口
*/
public interface AdminService {
    /**
    *
    * @title: 新增管理员
    * @parama:  Admin
    * @return: Admin
    */
    Boolean newAdmin(Admin admin);

    /**
    *
    * @title: 修改管理员信息
    * @parama:  Admin
    * @return:
    */
    Boolean updateAdmin(Admin admin);



    /**
    *
    * @title: 获取管理员列表
    *
     * @parama:  num
     * @parama:  seiz
    * @return: List<Admin>
    */
    PageInfo<List<Admin>> getAdminList(Admin admin, int num, int size);

    /**
    *
    * @title: 查询管理员
    * @parama:  Admin
    * @return: Admin
    */
    Admin getAdminById(Integer id);

    /**
    *
    * @title: 登录
    * @parama:  admin
    * @return:
    */
    Admin AdminLogin(@RequestBody Admin admin);
}

