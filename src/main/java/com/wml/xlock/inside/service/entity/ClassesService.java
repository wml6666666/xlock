package com.wml.xlock.inside.service.entity;

import com.github.pagehelper.PageInfo;
import com.wml.xlock.inside.bean.Classes;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @Author: ls
*
* @title: 班级业务类
*/
@Service
public interface ClassesService {
    /**
     *
     * @title: 新增班级员
     * @parama:  Classes
     * @return: Classes
     */
    Boolean newClasses(Classes classes);

    /**
     *
     * @title: 修改班级员信息
     * @parama:  Classes
     * @return:
     */
    Boolean updateClasses(Classes classes);

    /**
     *
     * @title: 获取班级员列表
     *
     * @parama:  num
     * @parama:  seiz
     * @return: List<Classes>
     */
    PageInfo<List<Classes>> getClassesList(Classes classes, int num, int size);

    /**
     *
     * @title: 查询（登录）班级员
     * @parama:  Classes
     * @return: Classes
     */
    Classes getClassesById(Integer id);

    /**
     *
     * @param ids id列表号
     * @return 删错结果
     */
    Boolean delClassesByIds(List<Integer> ids);
}
