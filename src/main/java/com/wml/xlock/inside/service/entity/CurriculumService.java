package com.wml.xlock.inside.service.entity;

import com.github.pagehelper.PageInfo;
import com.wml.xlock.inside.bean.Curriculum;

import java.util.List;

/**
* @Author: ls
*
* @title: 课程业务类
*/
public interface CurriculumService {
    /**
     *
     * @title: 新增课程
     * @parama:  Curriculum
     * @return: Curriculum
     */
    Boolean newCurriculum(Curriculum curriculum);

    /**
     *
     * @title: 修改课程信息
     * @parama:  Curriculum
     * @return:
     */
    Boolean updateCurriculum(Curriculum curriculum);

    /**
     *
     * @title: 获取课程列表
     *
     * @parama:  num
     * @parama:  seiz
     * @return: List<Curriculum>
     */
    PageInfo<List<Curriculum>> getCurriculumList(Curriculum curriculum, int num, int size);

    /**
     *
     * @title: 查询（登录）课程
     * @parama:  Curriculum
     * @return: Curriculum
     */
    Curriculum getCurriculumById(Integer id);

    /**
     * 删除课程
     * @param ids 需要删除的id列表
     * @return 删除结果 boolean
     */
    boolean delCurriculumsByIds(List<Integer> ids);
}
