package com.wml.xlock.inside.service.entity;

import com.github.pagehelper.PageInfo;
import com.wml.xlock.inside.bean.LockOpenNotes;

import java.util.List;

public interface LockOpenNotesService {
    /**
     * @Author 7221
     * @Description //TODO 分页查询开门记录
     * @Date 20:44 2019/6/20
     * @Param [current, size]
     * @return com.github.pagehelper.PageInfo<com.wml.xlock.inside.bean.LockOpenNotes>
     **/
    PageInfo<LockOpenNotes> getOpenNsBypage(Integer current,Integer size);

    /**
     * @Author 7221
     * @Description //TODO 添加开门记录
     * @Date 20:45 2019/6/20
     * @Param [lockOpenNotes]
     * @return boolean
     **/
    boolean addOpenN(LockOpenNotes lockOpenNotes);

    /**
     * @Author 7221
     * @Description //TODO  批量删除记录
     * @Date 20:46 2019/6/20
     * @Param [ids]
     * @return boolean
     **/
    boolean delOpenNByIds(List ids);

    
 }
