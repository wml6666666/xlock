package com.wml.xlock.inside.service.entity;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.github.pagehelper.PageInfo;
import com.wml.xlock.inside.bean.Lock;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

public interface LockService {

    /**
     * @Author 7221
     * @Description //TODO  分页查询锁记录
     * @Date 20:35 2019/6/20
     * @Param [current, size]
     * @return com.github.pagehelper.PageInfo<com.wml.xlock.inside.bean.Lock>
     **/
    PageInfo<Lock> getLocksBypage(Integer current, Integer size);

    /**
     * @Author 7221
     * @Description //TODO 绑定锁
     * @Date 20:38 2019/6/20
     * @Param [lock]
     * @return boolean
     **/
    boolean  addLock(Lock lock);

    boolean  bindLock_Room(Integer lock_id,Integer room_id);

    /**
     * @Author 7221
     * @Description //TODO  通过id获取锁信息
     * @Date 20:40 2019/6/20
     * @Param [id]
     * @return com.wml.xlock.inside.bean.Lock
     **/
    Lock getLockById(Integer id);

    /**
     * @Author 7221
     * @Description //TODO  修改锁名称
     * @Date 20:42 2019/6/20
     * @Param [id, newname]
     * @return boolean
     **/
    boolean uplockNameById(Lock lock);

    /**
     * @Author 7221
     * @Description //TODO  通过房间查询锁
     * @Date 15:26 2019/6/22
     * @Param [room_id]
     * @return java.util.List<com.wml.xlock.inside.bean.Lock>
     **/
    List<Lock> getLocksByRoomId(Integer room_id);


    /**
     * @Author 7221
     * @Description //TODO 修改锁状态
     * @Date 14:30 2019/6/23
     * @Param [imei, status]
     * @return boolean
     **/
    boolean uplockstatus(String imei,Integer status);


    /**
     *  对锁的信息进行修改（电量，信号 ---）
     * @param lockId
     * @param map 要修改的参数
     * @return
     */
    boolean syncLockInfoByLockId(Integer lockId, Map<String,Object> map);
}
