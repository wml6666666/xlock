package com.wml.xlock.inside.service.entity;

import com.github.pagehelper.PageInfo;
import com.wml.xlock.inside.bean.LockCpNotes;

import java.util.List;

public interface LockcpService {
    /**
     * @Author 7221
     * @Description //TODO 分页查询报警记录
     * @Date 20:44 2019/6/20
     * @Param [current, size]
     * @return com.github.pagehelper.PageInfo<com.wml.xlock.inside.bean.LockcpService>
     **/
    PageInfo<LockCpNotes> getlockcpBypage(Integer current, Integer size);

    /**
     * @Author 7221
     * @Description //TODO 添加报警记录
     * @Date 20:45 2019/6/20
     * @Param [lockOpenNotes]
     * @return boolean
     **/
    boolean addLockcpN(LockCpNotes lockcpNotes);

    /**
     * @Author 7221
     * @Description //TODO  批量删除记录
     * @Date 20:46 2019/6/20
     * @Param [ids]
     * @return boolean
     **/
    boolean delLockcpByIds(List ids);


}
