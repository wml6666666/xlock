package com.wml.xlock.inside.service.entity;

import com.github.pagehelper.PageInfo;
import com.wml.xlock.inside.bean.Lockpwd;

import java.util.List;

public interface LockpwdService {
    /**
     * @Author 7221
     * @Description //TODO 通过id查询密码
     * @Date 20:50 2019/6/20
     * @Param [id]
     * @return com.wml.xlock.inside.bean.Lockpwd
     **/
    Lockpwd getLockpwdById(Integer id);

    /**
     * @Author 7221
     * @Description //TODO 添加密码
     * @Date 20:52 2019/6/20
     * @Param [lockpwd]
     * @return boolean
     **/
    boolean addLockpwd(Lockpwd lockpwd);

    /**
     *  通过密码类型删除密码
     * @param type
     * @return
     */
    boolean delLockpwdByType(Integer type);

    /**
     *  分页查询锁的密码列表
     * @param lockId 锁id
     * @param type 密码类型 (1:指纹；2:密码，3、卡、4：临时密码 0 全部）
     * @param current 页数
     * @param size 页数据数
     * @return
     */
    PageInfo<Lockpwd> getLockpwdsByLockId(Integer lockId, Integer type, Integer current, Integer size);
}
