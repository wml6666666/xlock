package com.wml.xlock.inside.service.entity;

import com.github.pagehelper.PageInfo;
import com.wml.xlock.inside.bean.Room;
import io.swagger.models.auth.In;

import java.util.List;

/**
* @Author: ls
*
* @title: 房间业务类
*/
public interface RoomService {
    /**
     *
     * @title: 新增房间员
     * @parama:  Room
     * @return:
     */
    Boolean newRoom(Room room);

    /**
     *
     * @title: 修改房间员信息
     * @parama:  Room
     * @return:
     */
    Boolean updateRoom(Room room);

    /**
     *
     * @title: 获取房间员列表
     *
     * @parama:  num
     * @parama:  seiz
     * @return: List<Room>
     */
    PageInfo<List<Room>> getRoomList(Room room, int num, int size);

    /**
     *
     * @title: 查询（登录）房间员
     * @parama:  Room
     * @return: Room
     */
    Room getRoomById(Integer id);

    /**
     *
     * @param ids id 列表
     * @return 结果
     */
    boolean delClassesByIds(List<Integer> ids);
}
