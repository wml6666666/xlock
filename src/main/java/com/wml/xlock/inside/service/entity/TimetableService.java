package com.wml.xlock.inside.service.entity;

import com.github.pagehelper.PageInfo;
import com.wml.xlock.inside.bean.TimetableEntiy;
import com.wml.xlock.inside.bean.TimetableMiddble;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * @Author 7221
 * @Description //TODO  课表的数据处理层
 * @Date 20:31 2019/6/1
 **/
@CacheConfig(cacheNames = "timetible")
public interface TimetableService {
    /**
     * @Author 7221
     * @Description //TODO 插入数据
     * @Date 20:40 2019/6/1
     * @Param TimetableEntiy timetableEntiy
     * @return boolean
     **/
    @CacheEvict
     boolean insertTimetable(TimetableEntiy timetableEntiy);

    /**
     * @Author 7221
     * @Description //TODO 删除数据
     * @Date 20:43 2019/6/1
     * @Param [id]
     * @return java.lang.boolean
     **/
    @CacheEvict
    boolean deleteTimetableById(Integer id);
    
    /**
     * @Author 7221
     * @Description //TODO  修改数据
     * @Date 20:43 2019/6/1
     * @Param TimetableEntiy timetableEntiy
     * @return java.lang.boolean
     **/
    @CacheEvict
     boolean updateTimetableById(TimetableEntiy timetableEntiy);

    /**
     * @Author 7221
     * @Description //TODO 查询全部
     * @Date 20:43 2019/6/1
     * @Param []
     * @return java.util.List<com.wml.xlock.inside.bean.TimetableMiddble>
     **/
  //  PageInfo<TimetableMiddble> selectPageAll(Integer pagenum);

    /**
     * @Author 7221
     * @Description //TODO
     * @Date 13:51 2019/6/3
     * @Param [TimetableEntiy timetableEntiy]
     * @return com.github.pagehelper.PageInfo<com.wml.xlock.inside.bean.TimetableMiddble>
     **/
    @Cacheable(keyGenerator = "cacheKeyGenerator")
    List<TimetableMiddble> selectAll(TimetableEntiy timetableEntiy);

    /**
     * @Author 7221
     * @Description //TODO 根据传入的map动态查询
     * @Date 11:59 2019/6/3
     * @Param TimetableEntiy timetableEntiy,Integer pagenum
     * @return com.wml.xlock.inside.bean.TimetableMiddble
     **/
    @Cacheable(keyGenerator = "cacheKeyGenerator")
     PageInfo<TimetableMiddble> getTimetableByEntiy(TimetableEntiy timetableEntiy,Integer pagenum) ;

    /**
     * @Author 7221
     * @Description //TODO 批量删除
     * @Date 20:05 2019/6/11
     * @Param [list]
     * @return boolean
     **/
    @CacheEvict(allEntries=true)
     boolean deleteTimetableByList(List<Integer> list);

}
