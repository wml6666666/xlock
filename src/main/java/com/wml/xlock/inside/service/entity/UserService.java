package com.wml.xlock.inside.service.entity;

import com.github.pagehelper.PageInfo;
import com.wml.xlock.inside.bean.User;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
* @Author: ls
*
* @title: User业务类
*/
public interface UserService {
    /**
     *
     * @title: 新增User
     * @parama:  User
     * @return:
     */
    Boolean newUser(User user);

    /**
     *
     * @title: 修改User信息
     * @parama:  User
     * @return:
     */
    Boolean updateUser(User user);

    /**
     *
     * @title: 获取User列表
     *
     * @parama:  num
     * @parama:  seiz
     * @return: List<User>
     */
    PageInfo<List<User>> getUserList(User user, int num, int size);

    /**
     *
     * @title: 查询（登录）User
     * @parama:  User
     * @return: User
     */
    User getUserById(Integer id);

    /**
     *
     * @title: 登录
     * @parama:  User
     * @return:
     */
    User UserLogin(@RequestBody User user);
}
