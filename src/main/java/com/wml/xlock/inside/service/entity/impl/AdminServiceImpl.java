package com.wml.xlock.inside.service.entity.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wml.xlock.inside.bean.Admin;
import com.wml.xlock.inside.mapper.AdminMapper;
import com.wml.xlock.inside.service.entity.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@CacheConfig(cacheNames = "admin")
public class AdminServiceImpl implements AdminService {

    @Autowired
    private AdminMapper adminMapper;

    @Override
    @Transactional
    @CachePut
    public Boolean newAdmin(Admin admin) {
        int i = adminMapper.insertSelective(admin);
        return i>0;
    }

    @Override
    @Transactional
    @CachePut
    public Boolean updateAdmin(Admin admin) {
        int i = adminMapper.updateByPrimaryKeySelective(admin);
        return i>0;
    }


    @Override
    @Cacheable
    public PageInfo<List<Admin>> getAdminList(Admin admin,int num, int size) {
        PageHelper.startPage(num,size);
        List<Admin> admins = adminMapper.getAdminList(admin);
        PageInfo<List<Admin>> pageInfo=new PageInfo(admins);
        return pageInfo;
    }

    @Override
    @Cacheable
    public Admin getAdminById(Integer id) {
        return adminMapper.selectByPrimaryKey(id);
    }

    @Override
    @Cacheable
    public Admin AdminLogin(Admin admin) {
        return adminMapper.AdminLogin(admin);
    }
}
