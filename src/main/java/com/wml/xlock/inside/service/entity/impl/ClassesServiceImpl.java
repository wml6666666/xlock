package com.wml.xlock.inside.service.entity.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wml.xlock.inside.bean.Classes;
import com.wml.xlock.inside.mapper.ClassesMapper;
import com.wml.xlock.inside.service.entity.ClassesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@CacheConfig(cacheNames = "classes")
public class ClassesServiceImpl  implements ClassesService {

    @Autowired
    private ClassesMapper classesMapper;

    @Override
    @Transactional
    @CachePut
    public Boolean newClasses(Classes classes) {
        return classesMapper.insert(classes)>0;
    }

    @Override
    @Transactional
    @CachePut
    public Boolean updateClasses(Classes classes) {
        return classesMapper.updateById(classes)>0;
    }

    @Override
    @Cacheable
    public PageInfo<List<Classes>> getClassesList(Classes classes, int num, int size) {
        PageHelper.startPage(num,size);
        List<Classes> classesList = classesMapper.getClassesList(classes);
        PageInfo<List<Classes>> pageInfo=new PageInfo(classesList);
        return pageInfo;
    }

    @Override
    @Cacheable
    public Classes getClassesById(Integer id) {
        return classesMapper.selectById(id);
    }

    @Override
    @CacheEvict(allEntries=true)
    public Boolean delClassesByIds(List<Integer> ids) {
        return classesMapper.deleteBatchIds(ids)>0;
    }

}
