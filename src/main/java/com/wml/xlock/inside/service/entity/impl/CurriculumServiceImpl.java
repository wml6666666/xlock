package com.wml.xlock.inside.service.entity.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wml.xlock.inside.bean.Curriculum;
import com.wml.xlock.inside.mapper.CurriculumMapper;
import com.wml.xlock.inside.service.entity.CurriculumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@CacheConfig(cacheNames = "curriculum")
public class CurriculumServiceImpl implements CurriculumService {

    @Autowired
    private CurriculumMapper curriculumMapper;

    @Override
    @Transactional
    @CachePut
    public Boolean newCurriculum(Curriculum curriculum) {
        return curriculumMapper.insert(curriculum)>0;
    }

    @Override
    @Transactional
    @CachePut
    public Boolean updateCurriculum(Curriculum curriculum) {
        return curriculumMapper.updateById(curriculum)>0;
    }

    @Override
    @Cacheable
    public PageInfo<List<Curriculum>> getCurriculumList(Curriculum curriculum, int num, int size) {
        PageHelper.startPage(num,size);
        List<Curriculum> curriculumList =curriculumMapper.getCurriculumList(curriculum);
        PageInfo<List<Curriculum>> pageInfo=new PageInfo(curriculumList);
        return pageInfo;
    }

    @Override
    @Cacheable
    public Curriculum getCurriculumById(Integer id) {
        return curriculumMapper.selectById(id);
    }

    @Override
    public boolean delCurriculumsByIds(List<Integer> ids) {
        return curriculumMapper.deleteBatchIds(ids)>0;
    }
}
