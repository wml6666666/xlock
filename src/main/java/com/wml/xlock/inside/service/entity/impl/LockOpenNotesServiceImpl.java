package com.wml.xlock.inside.service.entity.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wml.xlock.inside.bean.LockOpenNotes;
import com.wml.xlock.inside.mapper.LockOpenNotesMappper;
import com.wml.xlock.inside.service.entity.LockOpenNotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class LockOpenNotesServiceImpl implements LockOpenNotesService {
    @Autowired
    LockOpenNotesMappper lockOpenNotesMappper;
    @Override
    public PageInfo<LockOpenNotes> getOpenNsBypage(Integer current, Integer size) {
        PageHelper.startPage(current,size);
        List<LockOpenNotes> lockOpenNotes = lockOpenNotesMappper.selectList(null);
        PageInfo pageInfo=new PageInfo(lockOpenNotes);
        return pageInfo;
    }

    @Override
    public boolean addOpenN(LockOpenNotes lockOpenNotes) {
        int insert = lockOpenNotesMappper.insert(lockOpenNotes);
        return insert>0;
    }

    @Override
    public boolean delOpenNByIds(List ids) {
        int i = lockOpenNotesMappper.deleteBatchIds(ids);
        return i>0;
    }
}
