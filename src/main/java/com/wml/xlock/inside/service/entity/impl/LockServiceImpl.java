package com.wml.xlock.inside.service.entity.impl;


import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wml.xlock.inside.bean.Lock;
import com.wml.xlock.inside.bean.Lockpwd;
import com.wml.xlock.inside.mapper.LockMapper;
import com.wml.xlock.inside.service.entity.LockService;
import com.wml.xlock.inside.service.entity.LockpwdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class LockServiceImpl implements LockService {
    @Autowired
    LockMapper lockMapper;
    @Autowired
    LockpwdService lockpwdService;
    @Override
    public PageInfo<Lock> getLocksBypage(Integer current, Integer size) {
        PageHelper.startPage(current,size);
        List<Lock> locks = lockMapper.selectList(null);
//        for (Lock lock:
//             locks) {
//            List<Lockpwd> lockpwdsByLockId = lockpwdService.getLockpwdsByLockId(lock.getId(),0,1,1000).getList();
//            lock.setLockpwdList(lockpwdsByLockId);
//        }
        PageInfo<Lock> pageInfo=new PageInfo<>(locks);
        return pageInfo;
    }

    @Override
    public boolean addLock(Lock lock) {
        int insert = lockMapper.insert(lock);
        return insert>0;
    }

    @Override
    public boolean bindLock_Room(Integer lock_id, Integer room_id) {
        Lock lock=new Lock();
        lock.setRoom_id(room_id);
        lock.setId(lock_id);
        int i = lockMapper.updateById(lock);
        return i>0;
    }


    @Override
    public Lock getLockById(Integer id) {
        Lock lock = lockMapper.selectById(id);
        return lock;
    }

    @Override
    public boolean uplockNameById(Lock lock) {
        int i = lockMapper.updateById(lock);
        return i>0;
    }

    @Override
    public List<Lock> getLocksByRoomId(Integer room_id) {
        Map map=new HashMap();
        map.put("lock_roomId",room_id);
        List list = lockMapper.selectByMap(map);
        return list;
    }
    @Override
    public boolean uplockstatus(String imei, Integer status) {
        Lock lock=new Lock();
        lock.setStatus(status);
        lock.setUpdateTime(new Date());
        UpdateWrapper updateWrapper=new UpdateWrapper();
        updateWrapper.eq("lock_imei",imei);
        return lockMapper.update(lock, updateWrapper)>0;
    }

    @Override
    public boolean syncLockInfoByLockId(Integer lockId, Map<String,Object> map) {
        UpdateWrapper updateWrapper=new UpdateWrapper();
        updateWrapper.eq("lock_id",lockId);
        updateWrapper.set("lock_updateTime",new Date());
        for (String key:
            map.keySet() ) {
            updateWrapper.set(key,map.get(key));
        }
        return lockMapper.update(null, updateWrapper)>0;
    }
}
