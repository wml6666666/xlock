package com.wml.xlock.inside.service.entity.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wml.xlock.inside.bean.Lock;
import com.wml.xlock.inside.bean.LockCpNotes;
import com.wml.xlock.inside.mapper.LockcpMapper;
import com.wml.xlock.inside.service.entity.LockcpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service("inLockcpServiceImpl")
@Transactional
public class LockcpServiceImpl implements LockcpService {
    @Autowired
    LockcpMapper lockcpMapper;
    @Override
    public PageInfo<LockCpNotes> getlockcpBypage(Integer current, Integer size) {
        PageHelper.startPage(current,size);
        List<LockCpNotes> lockcpNotes = lockcpMapper.selectList(null);
        PageInfo<LockCpNotes> pageInfo=new PageInfo(lockcpNotes);
        return pageInfo;
    }

    @Override
    public boolean addLockcpN(LockCpNotes lockcpNotes) {
        int insert = lockcpMapper.insert(lockcpNotes);
        return insert>0;
    }

    @Override
    public boolean delLockcpByIds(List ids) {
        int i = lockcpMapper.deleteBatchIds(ids);
        return i>0;
    }


}
