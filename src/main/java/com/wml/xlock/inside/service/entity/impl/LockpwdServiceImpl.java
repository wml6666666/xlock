package com.wml.xlock.inside.service.entity.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wml.xlock.inside.bean.Lockpwd;
import com.wml.xlock.inside.mapper.LockpwdMapper;
import com.wml.xlock.inside.service.entity.LockpwdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class LockpwdServiceImpl implements LockpwdService {
    @Autowired
    LockpwdMapper lockpwdMapper;
    @Override
    public Lockpwd getLockpwdById(Integer id) {
        Lockpwd lockpwd = lockpwdMapper.selectById(id);
        return lockpwd;
    }

    @Override
    public boolean addLockpwd(Lockpwd lockpwd) {
        int insert = lockpwdMapper.insert(lockpwd);
        return insert>0;
    }

    @Override
    public boolean delLockpwdByType(Integer type) {
        Map map=new HashMap(1);
        map.put("lockpwd_type",type);
        return  lockpwdMapper.deleteByMap(map)>0;

    }


    @Override
    public PageInfo<Lockpwd> getLockpwdsByLockId(Integer lockId,Integer type,Integer current,Integer size) {
        Map<String,Object> map=new HashMap();
        map.put("lockpwd_lockId",lockId);
        if(0!=type){
            map.put("lockpwd_type",type);
        }
        PageHelper.startPage(current,size);
        List<Lockpwd> lockpwds = lockpwdMapper.selectByMap(map);
        PageInfo<Lockpwd> pageInfo=new PageInfo(lockpwds);
        return pageInfo;
    }
}
