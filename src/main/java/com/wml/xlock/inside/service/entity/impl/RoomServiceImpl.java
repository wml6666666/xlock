package com.wml.xlock.inside.service.entity.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wml.xlock.inside.bean.Room;
import com.wml.xlock.inside.mapper.RoomMapper;
import com.wml.xlock.inside.service.entity.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@CacheConfig(cacheNames = "room")
public class RoomServiceImpl  implements RoomService {

    @Autowired
    private RoomMapper roomMapper;

    @Override
    @Transactional
    @CachePut
    public Boolean newRoom(Room room) {
        return roomMapper.insert(room)>0;
    }

    @Override
    @Transactional
    @CachePut
    public Boolean updateRoom(Room room) {
        return roomMapper.updateById(room)>0;
    }

    @Override
    @Cacheable
    public PageInfo<List<Room>> getRoomList(Room room, int num, int size) {
        PageHelper.startPage(num,size);
        List<Room> rooms =roomMapper.getRoomList(room);
        PageInfo<List<Room>> pageInfo=new PageInfo(rooms);
        return pageInfo;
    }

    @Override
    @Cacheable
    public Room getRoomById(Integer id) {
        return roomMapper.selectById(id);
    }

    @Override
    public boolean delClassesByIds(List<Integer> ids) {
        return roomMapper.deleteBatchIds(ids)>0;
    }
}
