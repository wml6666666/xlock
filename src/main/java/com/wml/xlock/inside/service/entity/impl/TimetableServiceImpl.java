package com.wml.xlock.inside.service.entity.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wml.xlock.inside.bean.TimetableEntiy;
import com.wml.xlock.inside.bean.TimetableMiddble;
import com.wml.xlock.inside.config.PagePropeties;
import com.wml.xlock.inside.mapper.TimetableMapper;
import com.wml.xlock.inside.service.entity.TimetableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TimetableServiceImpl implements TimetableService {
    @Autowired
    TimetableMapper timetableMapper;
    @Autowired
    PagePropeties pagePropeties;

    @Override
    public boolean insertTimetable(TimetableEntiy timetableEntiy) {
        return timetableMapper.insert(timetableEntiy)>0;
    }

    @Override
    public boolean deleteTimetableById(Integer id) {
        return timetableMapper.deleteById(id)>0;
    }

    @Override
    public boolean updateTimetableById(TimetableEntiy timetableEntiy) {
        return timetableMapper.updateById(timetableEntiy)>0;
    }

//    @Override
//    public PageInfo<TimetableMiddble> selectPageAll(Integer pagenum) {
//        PageHelper.startPage(pagenum,pagePropeties.getPageSize());
//        List<TimetableMiddble> timetableMiddbles = timetableMapper.selectAll();
//        PageInfo<TimetableMiddble> pageInfo=new PageInfo<TimetableMiddble>(timetableMiddbles);
//        return pageInfo;
//    }

    @Override
    public List<TimetableMiddble> selectAll(TimetableEntiy timetableEntiy) {
        List<TimetableMiddble> timetableMiddbleall = timetableMapper.getTimetableByEntiy(timetableEntiy);
        return timetableMiddbleall;
    }


    @Override
    public PageInfo<TimetableMiddble> getTimetableByEntiy(TimetableEntiy timetableEntiy,Integer pagenum) {
        PageHelper.startPage(pagenum,pagePropeties.getPageSize());
        List<TimetableMiddble> timetableMiddbleByMap = timetableMapper.getTimetableByEntiy(timetableEntiy);
        PageInfo<TimetableMiddble> pageInfo=new PageInfo<TimetableMiddble>(timetableMiddbleByMap);
        return pageInfo;
    }

    @Override
    public boolean deleteTimetableByList(List<Integer> list) {
        return  timetableMapper.deleteBatchIds(list)>0;
    }

}
