package com.wml.xlock.inside.service.entity.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wml.xlock.inside.bean.User;
import com.wml.xlock.inside.mapper.UserMapper;
import com.wml.xlock.inside.service.entity.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@CacheConfig(cacheNames = "user")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    @Transactional
    @CachePut
    public Boolean newUser(User user) {
        return userMapper.insert(user)>0;
    }

    @Override
    @Transactional
    @CachePut
    public Boolean updateUser(User user) {
        return userMapper.updateByPrimaryKeySelective(user)>0;
    }

    @Override
    @Cacheable
    public PageInfo<List<User>> getUserList(User user, int num, int size) {
        PageHelper.startPage(num,size);
        List<User> users =userMapper.getUserList(user);
        PageInfo<List<User>> pageInfo=new PageInfo(users);
        return pageInfo;
    }

    @Override
    @Cacheable
    public User getUserById(Integer id) {
        return userMapper.selectByPrimaryKey(id);
    }

    @Override
    @Cacheable
    public User UserLogin(User user) {
        return userMapper.UserLogin(user);
    }
}
