package com.wml.xlock.interceptor;

import com.wml.xlock.inside.bean.Admin;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Component
public class LoginInterceptor implements HandlerInterceptor{
    /**
     * @Author 7221
     * @Description //TODO 判断用户是否登陆过
     * @Date 20:02 2019/6/11
     * @Param [request, response, handler]
     * @return boolean
     **/
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String requestURI = request.getRequestURI();
//如果获取到的这个url路径是在查询后台数据的方法的时候,进行拦截
        HttpSession session = request.getSession();
        Admin admin = (Admin) request.getAttribute("admin");
        if(admin!=null){
            //登陆成功的用户
            return true;
        }else{
            //没有登陆，转向登陆界面
            return false;
        }
    }



    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
