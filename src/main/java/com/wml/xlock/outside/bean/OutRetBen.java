package com.wml.xlock.outside.bean;

import java.util.Map;

public class OutRetBen<T> {
    private  Integer  code;//状态码
    private  String msg;//状态码说明
    private  String actionCode;
    private  String actionUri;
    private T data; //返回数据

    public OutRetBen(Integer code, String msg, String actionCode, String actionUri, T data) {
        this.code = code;
        this.msg = msg;
        this.actionCode = actionCode;
        this.actionUri = actionUri;
        this.data = data;
    }

    public OutRetBen() {
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getActionCode() {
        return actionCode;
    }

    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }

    public String getActionUri() {
        return actionUri;
    }

    public void setActionUri(String actionUri) {
        this.actionUri = actionUri;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "OutRetBen{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", actionCode='" + actionCode + '\'' +
                ", actionUri='" + actionUri + '\'' +
                ", data=" + data +
                '}';
    }
}
