package com.wml.xlock.outside.bean;

import java.util.Map;

public class PushBean {
    private  String imei; //imei号
    private  String createTime; //创建时间
    private  String pushType ;//推送类型
    private  String pushTypeDesc;//推送类型说明
    private Map<String,Object> pushContent;//推送具体数据

    public PushBean() {
    }

    public PushBean(String imei, String createTime, String pushType, String pushTypeDesc, Map<String, Object> pushContent) {
        this.imei = imei;
        this.createTime = createTime;
        this.pushType = pushType;
        this.pushTypeDesc = pushTypeDesc;
        this.pushContent = pushContent;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getPushType() {
        return pushType;
    }

    public void setPushType(String pushType) {
        this.pushType = pushType;
    }

    public String getPushTypeDesc() {
        return pushTypeDesc;
    }

    public void setPushTypeDesc(String pushTypeDesc) {
        this.pushTypeDesc = pushTypeDesc;
    }

    public Map<String, Object> getPushContent() {
        return pushContent;
    }

    public void setPushContent(Map<String, Object> pushContent) {
        this.pushContent = pushContent;
    }

    @Override
    public String toString() {
        return "PushBean{" +
                "imei='" + imei + '\'' +
                ", createTime='" + createTime + '\'' +
                ", pushType='" + pushType + '\'' +
                ", pushTypeDesc='" + pushTypeDesc + '\'' +
                ", pushContent=" + pushContent +
                '}';
    }
}
