package com.wml.xlock.outside.common;

/**
 * 全局配置enum
 */
public enum CommonSet {
    // 服务器url
    OVERALLURL("http://device.moozun.cn/api/open/"),
    // token
    TOKEN("2e718d307981d74a20a2a4abb2eac883");
    private  String value;
    CommonSet(String value) {
        this.value=value;
    }
    public String getValue() {
        return value;
    }
}
