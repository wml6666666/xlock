package com.wml.xlock.outside.controller;

import com.wml.xlock.inside.bean.Lock;
import com.wml.xlock.inside.bean.LockCpNotes;
import com.wml.xlock.inside.bean.LockOpenNotes;
import com.wml.xlock.inside.bean.Lockpwd;
import com.wml.xlock.inside.service.entity.LockOpenNotesService;
import com.wml.xlock.inside.service.entity.LockcpService;
import com.wml.xlock.inside.service.entity.LockpwdService;
import com.wml.xlock.outside.bean.OutRetBen;
import com.wml.xlock.outside.bean.PushBean;
import com.wml.xlock.outside.service.LockService;
import com.wml.xlock.outside.util.StringDateFormate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController("outLockController")
@RequestMapping("/outside/lock")
public class LockController {

    @Autowired
    LockService lockService;
    @Autowired
    com.wml.xlock.inside.service.entity.LockService lockinService;
    @Autowired
    LockpwdService lockpwdService;
    @Autowired
    LockOpenNotesService lockOpenNotesService;
    @Autowired
    LockcpService lockcpService;

    /**
     *  绑定设备
     * @param imei imei号
     * @return
     */
    @PostMapping("/bind")
    public Object lockbind(@RequestParam String imei){
        Map map=new HashMap();
        OutRetBen lockbind = lockService.lockbind(imei);
        map.put("bindresult",lockbind.getMsg());
        if("success".equals(lockbind.getMsg())){
            OutRetBen<Map> onelockingfo = lockService.getonelockingfo(imei);
            Map data = onelockingfo.getData();
            Lock lock=new Lock((Integer) data.get("lockId"),data.get("lockName").toString(),imei,null,null,(Integer) data.get("lockStatus"),(Integer) data.get("lockButtPlatformType"),data.get("lockModel").toString(),(Integer) data.get("lockBatteryLevel"),(Integer) data.get("lockSignalLevel"),(Long)data.get("lockDeviceId"),(Integer) data.get("lockDeviceType"),data.get("lockRemark").toString(),null);
            boolean b = lockinService.addLock(lock);
            if(!b){
                map.put("dbresult","error");
            }
            else{
                map.put("dbresult","success");
            }
        }
        return  map;
    }
    /**
     *
     * @param imei imei号
     * @return
     */
    @PostMapping("/one/imei")
    public Object getonelockingfo(@RequestParam String imei){
        return lockService.getonelockingfo(imei);
    }
    /**
     *
     * @param lockid 设备Id
     * @param type  密码类型（0:所有；1:指纹；2:密码;3:卡;4:临时密码；）
     * @param current 当前页
     * @param size 每页记录
     * @return
     */
    @PostMapping("/password/list")
    public Object getlockpassword(@RequestParam Integer lockid,@RequestParam Integer type,@RequestParam(required = false, defaultValue = "1") Integer current,@RequestParam Integer size){
        return  lockService.getlockpassword(lockid,type,current,size);
    }

    /**
     *  设置锁密码
     * @param lockId 锁id
     * @param pwdName  密码名字
     * @param pwdContent 密码内容
     * @param pwdStartTime 开始时间
     * @param pwdEndTime 结束时间
     * @return
     */
    @PostMapping("/password/temporary")
    public Object setlocktemppwd(@RequestParam Integer lockId,@RequestParam String pwdName,@RequestParam  String pwdContent,@RequestParam String pwdStartTime,@RequestParam String pwdEndTime){
        Map map=new HashMap();
        OutRetBen<Map> setlocktemppwd= lockService.setlocktemppwd(lockId, pwdName, pwdContent, pwdStartTime, pwdEndTime);
        map.put("outresult",setlocktemppwd.getMsg());
        if("success".equals(setlocktemppwd.getMsg())){
            Lockpwd lockpwd=new Lockpwd((Integer) setlocktemppwd.getData().get("pwdId"),lockId,null,null,pwdName,pwdContent,4, StringDateFormate.parseDate(pwdStartTime),StringDateFormate.parseDate(pwdEndTime),new Date(),new Date(),0,2,0);
            boolean b = lockpwdService.addLockpwd(lockpwd);
            if(b){
                map.put("dbresult","success");
            }
            else{
                map.put("dbresult","error");
            }
        }
        return map;
    }

    /**
     *  开门记录推送
     * @param pushBean
     */
    @PostMapping("record/open")
    public void lockopenrecord(@RequestBody PushBean pushBean){
        LockOpenNotes lockOpenNote=new LockOpenNotes(pushBean.getImei(),StringDateFormate.parseDate(pushBean.getCreateTime()),(Integer) pushBean.getPushContent().get("passwordId"));
        lockOpenNotesService.addOpenN(lockOpenNote);
    }
    /**
     *  报警记录推送
     * @param pushBean
     */
    @PostMapping("record/alarm")
    public void lockalarmrecord(@RequestBody PushBean pushBean){
        LockCpNotes lockCpNotes=new LockCpNotes(pushBean.getImei(),StringDateFormate.parseDate(pushBean.getCreateTime()),(Integer) pushBean.getPushContent().get("alarmType"));
        lockcpService.addLockcpN(lockCpNotes);
    }
    /**
     *  锁状态记录推送
     * @param pushBean
     */
    @PostMapping("/status")
    public void lockstatus(@RequestBody PushBean pushBean){
        lockinService.uplockstatus(pushBean.getImei(),(Integer) pushBean.getPushContent().get("deviceStatusType"));
    }

//    /**
//     *  同步密码
//     * @param lockId
//     * @return
//     */
//    @PostMapping("/sychLock")
//    public Object sychLock(@RequestParam Integer lockId,String pwdname){
//        Map map=new HashMap();
//        OutRetBen outRetBen = lockService.sychLock(lockId);
//        map.put("outresult",outRetBen.getMsg());
//        if("success".equals(outRetBen.getMsg())){
//            Map data = outRetBen.getData();
//            Lockpwd lockpwd=new Lockpwd((Integer) data.get("pwdId"),lockId,(Long)data.get("pwdDevicePwdId"),(Long)data.get("pwdLockDeviceId"),data.get("pwdName").toString(),data.get("pwdContent").toString(),(Integer) data.get("pwdType"), StringDateFormate.parseDate(data.get("pwdStartTime").toString()),StringDateFormate.parseDate(data.get("pwdEndTime").toString()),StringDateFormate.parseDate(data.get("pwdCreateTime").toString()),StringDateFormate.parseDate(data.get("pwdUpdateTime").toString()),0,2,0);
//            boolean b = lockpwdService.addLockpwd(lockpwd);
//            if(!b){
//                map.put("dbresult","error");
//            }
//            else{
//                map.put("dbresult","success");
//            }
//        }
//        return  map;
//    }



}
