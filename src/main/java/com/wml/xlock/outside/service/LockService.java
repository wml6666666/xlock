package com.wml.xlock.outside.service;

import com.wml.xlock.inside.bean.Lock;
import com.wml.xlock.outside.bean.OutRetBen;

import java.util.List;
import java.util.Map;

public interface LockService {
     /**
      *  绑定锁
      * @param imei 锁imei
      * @return
      */
     OutRetBen lockbind(String imei);

     /**
      *  根据锁imei得到锁信息
      * @param imei 锁imei
      * @return
      */
     OutRetBen getonelockingfo( String imei);

     /**
      * 得到锁密码
      * @param lockid 设备Id
      * @param type  密码类型（0:所有；1:指纹；2:密码;3:卡;4:临时密码；）
      * @param current 当前页
      * @param size 每页记录
      * @return
      */
     OutRetBen<List<Map>> getlockpassword(Integer lockid,Integer type,Integer current, Integer size);

     /**
      *  设置锁密码
      * @param lockId 锁id
      * @param pwdName  密码名字
      * @param pwdContent 密码内容
      * @param pwdStartTime 开始时间
      * @param pwdEndTime 结束时间
      * @return
      */
     OutRetBen setlocktemppwd(Integer lockId,String pwdName,String pwdContent,String pwdStartTime,String pwdEndTime);

     /**
      * @Author 7221
      * @Description //TODO 获取所有锁
      * @Date 11:35 2019/6/23
      * @Param []
      * @return java.lang.Object
      **/
     OutRetBen<List<Map>>  getallLock();

     /**
      * 同步锁的密码（可理解为返回锁的所有密码）
      * @param lockId
      * @return
      */
     OutRetBen<List<Map>> sychLock(Integer lockId);

}
