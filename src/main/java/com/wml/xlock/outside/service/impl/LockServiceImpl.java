package com.wml.xlock.outside.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.wml.xlock.inside.bean.Lock;
import com.wml.xlock.outside.bean.OutRetBen;
import com.wml.xlock.outside.common.CommonSet;
import com.wml.xlock.outside.service.LockService;
import com.wml.xlock.outside.util.HttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.List;
import java.util.Map;

@Service("outLockServiceImpl")
public class LockServiceImpl implements LockService {
    @Autowired
    HttpClient httpClient;

    @Override
    public OutRetBen lockbind(String imei) {
        MultiValueMap multiValueMap=new LinkedMultiValueMap();
        multiValueMap.add("imei",imei);
        String s = httpClient.sendPostRequest(CommonSet.OVERALLURL.getValue() + "/lock/bind", multiValueMap, null);
        OutRetBen outRetBen = JSONObject.parseObject(s, OutRetBen.class);
        return outRetBen;
    }

    @Override
    public OutRetBen getonelockingfo(String imei) {
        MultiValueMap multiValueMap=new LinkedMultiValueMap();
        multiValueMap.add("imei",imei);
        String s = httpClient.sendGetRequest(CommonSet.OVERALLURL.getValue() + "/lock/one/imei", multiValueMap, null);
        OutRetBen<Map> outRetBen = JSONObject.parseObject(s, OutRetBen.class);
        return outRetBen;
    }

    @Override
    public OutRetBen<List<Map>> getlockpassword(Integer lockid, Integer type, Integer current, Integer size) {
        MultiValueMap multiValueMap=new LinkedMultiValueMap();
        multiValueMap.add("lockId",lockid);
        multiValueMap.add("type",type);
        multiValueMap.add("current",current);
        multiValueMap.add("size",size);
        String s = httpClient.sendGetRequest(CommonSet.OVERALLURL.getValue() + "/lock/password/list", multiValueMap, null);
        OutRetBen<List<Map>> outRetBen = JSONObject.parseObject(s, OutRetBen.class);
        return outRetBen;
    }

    @Override
    public OutRetBen setlocktemppwd(Integer lockId, String pwdName, String pwdContent, String pwdStartTime, String pwdEndTime) {
        MultiValueMap multiValueMap=new LinkedMultiValueMap();
        multiValueMap.add("pwdLockId",lockId);
        multiValueMap.add("pwdName",pwdName);
        multiValueMap.add("pwdContent",pwdContent);
        multiValueMap.add("pwdStartTime",pwdStartTime);
        multiValueMap.add("pwdEndTime",pwdEndTime);
        String s = httpClient.sendPostRequest(CommonSet.OVERALLURL.getValue() + "/lock/password/temporary", multiValueMap, null);
        OutRetBen<Map> outRetBen = JSONObject.parseObject(s, OutRetBen.class);
        return outRetBen;
    }

    @Override
    public OutRetBen<List<Map>>  getallLock() {
        MultiValueMap multiValueMap=new LinkedMultiValueMap();
        multiValueMap.add("current",1);
        multiValueMap.add("size",1000);
        String s = httpClient.sendGetRequest(CommonSet.OVERALLURL.getValue() + "/lock/list", multiValueMap, null);
        OutRetBen<List<Map>> outRetBen = JSONObject.parseObject(s, OutRetBen.class);
        return outRetBen;
    }

    @Override
    public OutRetBen<List<Map>> sychLock(Integer lockId) {
        MultiValueMap multiValueMap=new LinkedMultiValueMap();
        multiValueMap.add("lockId",lockId);
        String s = httpClient.sendPostRequest(CommonSet.OVERALLURL.getValue() + "/lock/password/sync", multiValueMap, null);
        OutRetBen<List<Map>> outRetBen = JSONObject.parseObject(s, OutRetBen.class);
        return outRetBen;
    }


}
