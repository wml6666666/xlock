package com.wml.xlock.outside.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StringDateFormate {

    static  SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static Date parseDate(String dates){
        try {
            return  simpleDateFormat.parse(dates);
        } catch (ParseException e) {
            throw new RuntimeException(e.getMessage());
        }
    }
    public static String formatDate(Date date){
        try {
            return  simpleDateFormat.format(date);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}
