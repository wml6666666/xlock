/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50724
Source Host           : localhost:3306
Source Database       : xlock

Target Server Type    : MYSQL
Target Server Version : 50724
File Encoding         : 65001

Date: 2019-12-06 10:05:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_admin
-- ----------------------------
DROP TABLE IF EXISTS `sys_admin`;
CREATE TABLE `sys_admin` (
  `admin_id` int(11) NOT NULL DEFAULT '0',
  `admin_name` varchar(15) NOT NULL DEFAULT '',
  `admin_password` varchar(32) NOT NULL DEFAULT '',
  `admin_mail` varchar(50) DEFAULT NULL,
  `admin_phone` varchar(15) DEFAULT NULL,
  `admin_createTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `admin_updateTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `admin_status` int(1) NOT NULL,
  `admin_level` int(1) DEFAULT NULL,
  PRIMARY KEY (`admin_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=gbk;

-- ----------------------------
-- Records of sys_admin
-- ----------------------------
INSERT INTO `sys_admin` VALUES ('0', 'dd', 'ww', 'aa', '11', '2019-12-06 09:55:38', '2019-12-06 09:58:25', '1', null);

-- ----------------------------
-- Table structure for t_card
-- ----------------------------
DROP TABLE IF EXISTS `t_card`;
CREATE TABLE `t_card` (
  `card_id` int(11) NOT NULL AUTO_INCREMENT,
  `card_namenum` varchar(32) NOT NULL,
  `card_cretime` datetime NOT NULL,
  `lock_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`card_id`),
  KEY `lock_index` (`lock_id`) USING BTREE,
  KEY `user_index` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_card
-- ----------------------------

-- ----------------------------
-- Table structure for t_class
-- ----------------------------
DROP TABLE IF EXISTS `t_class`;
CREATE TABLE `t_class` (
  `class_id` int(11) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_class
-- ----------------------------
INSERT INTO `t_class` VALUES ('1', '软件1224');

-- ----------------------------
-- Table structure for t_curriculum
-- ----------------------------
DROP TABLE IF EXISTS `t_curriculum`;
CREATE TABLE `t_curriculum` (
  `curriculum_id` int(11) NOT NULL AUTO_INCREMENT,
  `curriculum_name` varchar(64) DEFAULT NULL,
  `curriculum_explain` text,
  PRIMARY KEY (`curriculum_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_curriculum
-- ----------------------------
INSERT INTO `t_curriculum` VALUES ('1', '语文', '汉语学习');

-- ----------------------------
-- Table structure for t_lock
-- ----------------------------
DROP TABLE IF EXISTS `t_lock`;
CREATE TABLE `t_lock` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lock_id` int(11) NOT NULL,
  `lock_name` varchar(64) DEFAULT NULL,
  `lock_imei` char(15) NOT NULL,
  `lock_createTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `lock_updateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `lock_status` int(11) DEFAULT NULL,
  `lock_buttPlatformType` int(11) DEFAULT NULL,
  `lock_model` varchar(32) DEFAULT NULL,
  `lock_batteryLevel` int(11) DEFAULT NULL,
  `lock_signalLevel` int(11) DEFAULT NULL,
  `lock_deviceId` bigint(20) DEFAULT NULL,
  `lock_deviceType` int(11) DEFAULT NULL,
  `lock_remark` varchar(32) DEFAULT NULL,
  `lock_roomId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `t_lock_imei` (`lock_imei`) USING BTREE,
  KEY `t_lock` (`lock_roomId`) USING BTREE,
  KEY `lock_id` (`lock_id`),
  CONSTRAINT `t_lock_ibfk_1` FOREIGN KEY (`lock_roomId`) REFERENCES `t_room` (`room_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_lock
-- ----------------------------
INSERT INTO `t_lock` VALUES ('1', '13', '1201', '359689090807535', '2019-05-17 01:56:55', '2019-06-25 11:33:18', '0', '0', '', '60', '1', '8883642005127168', '0', '家庭版：中国电信', '1');

-- ----------------------------
-- Table structure for t_lockcpnotes
-- ----------------------------
DROP TABLE IF EXISTS `t_lockcpnotes`;
CREATE TABLE `t_lockcpnotes` (
  `lockCpNotes_id` int(11) NOT NULL AUTO_INCREMENT,
  `lockCpNotes_imei` char(15) DEFAULT NULL,
  `lockCpNotes_createTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `lockCpNotes_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`lockCpNotes_id`),
  KEY `t_lockCpNotes` (`lockCpNotes_imei`),
  CONSTRAINT `t_lockcpnotes_ibfk_1` FOREIGN KEY (`lockCpNotes_imei`) REFERENCES `t_lock` (`lock_imei`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_lockcpnotes
-- ----------------------------

-- ----------------------------
-- Table structure for t_lockopennotes
-- ----------------------------
DROP TABLE IF EXISTS `t_lockopennotes`;
CREATE TABLE `t_lockopennotes` (
  `lockOpNos_id` int(11) NOT NULL AUTO_INCREMENT,
  `lockOpNos_imei` char(15) DEFAULT NULL,
  `lockOpNos_createTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `lockOpNos_lockpwdId` int(11) DEFAULT NULL,
  PRIMARY KEY (`lockOpNos_id`),
  KEY `t_lockopennotes_imei` (`lockOpNos_imei`),
  KEY `t_lockopennotes_ibfk_1` (`lockOpNos_lockpwdId`),
  CONSTRAINT `t_lockopennotes_ibfk_1` FOREIGN KEY (`lockOpNos_lockpwdId`) REFERENCES `t_lockpwd` (`lockpwd_id`),
  CONSTRAINT `t_lockopennotes_imei` FOREIGN KEY (`lockOpNos_imei`) REFERENCES `t_lock` (`lock_imei`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_lockopennotes
-- ----------------------------

-- ----------------------------
-- Table structure for t_lockpwd
-- ----------------------------
DROP TABLE IF EXISTS `t_lockpwd`;
CREATE TABLE `t_lockpwd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lockpwd_id` int(11) NOT NULL,
  `lockpwd_lockId` int(11) DEFAULT NULL,
  `lockpwd_devicePwdId` bigint(20) DEFAULT NULL,
  `lockpwd_lockDeviceId` bigint(20) DEFAULT NULL,
  `lockpwd_name` varchar(32) DEFAULT NULL,
  `lockpwd_content` varchar(32) DEFAULT NULL,
  `lockpwd_type` int(11) DEFAULT NULL,
  `lockpwd_startTime` datetime NOT NULL,
  `lockpwd_endTime` datetime NOT NULL,
  `lockpwd_createTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lockpwd_updateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lockpwd_status` int(11) DEFAULT NULL,
  `lockpwd_auth` int(11) DEFAULT NULL,
  `lockpwd_unlockTimes` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `t_lockpwd_id` (`lockpwd_id`),
  KEY `t_lockpwd_ibfk_1` (`lockpwd_lockId`),
  CONSTRAINT `t_lockpwd_ibfk_1` FOREIGN KEY (`lockpwd_lockId`) REFERENCES `t_lock` (`lock_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_lockpwd
-- ----------------------------
INSERT INTO `t_lockpwd` VALUES ('1', '53', '13', '2448597480439808', '8883642005127168', '7221', '147147', '4', '2019-06-23 16:56:58', '2019-07-15 17:05:58', '2019-06-25 11:47:11', '2019-06-25 11:47:11', '0', '2', '0');
INSERT INTO `t_lockpwd` VALUES ('3', '69', '13', null, null, '7221', '123456', '4', '2019-12-02 11:00:00', '2019-12-02 19:00:00', '2019-12-02 19:09:29', '2019-12-02 19:09:29', '0', '2', '0');
INSERT INTO `t_lockpwd` VALUES ('4', '74', '13', null, null, '7221', '123456', '4', '2019-12-02 11:00:00', '2019-12-02 19:00:00', '2019-12-02 19:36:43', '2019-12-02 19:36:43', '0', '2', '0');

-- ----------------------------
-- Table structure for t_room
-- ----------------------------
DROP TABLE IF EXISTS `t_room`;
CREATE TABLE `t_room` (
  `room_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_name` varchar(64) DEFAULT NULL,
  `room_floor` varchar(64) DEFAULT NULL,
  `room_num` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`room_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_room
-- ----------------------------
INSERT INTO `t_room` VALUES ('1', '3号楼', '2', '305');

-- ----------------------------
-- Table structure for t_timetable
-- ----------------------------
DROP TABLE IF EXISTS `t_timetable`;
CREATE TABLE `t_timetable` (
  `timetable_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `curriculum_id` int(11) DEFAULT NULL,
  `timetable_time` time DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `timetable_weekday` int(11) NOT NULL,
  PRIMARY KEY (`timetable_id`),
  KEY `ref_room` (`room_id`),
  KEY `ref_curriculum` (`curriculum_id`),
  KEY `ref_class` (`class_id`),
  KEY `ref_user` (`user_id`),
  CONSTRAINT `ref_class` FOREIGN KEY (`class_id`) REFERENCES `t_class` (`class_id`),
  CONSTRAINT `ref_curriculum` FOREIGN KEY (`curriculum_id`) REFERENCES `t_curriculum` (`curriculum_id`),
  CONSTRAINT `ref_room` FOREIGN KEY (`room_id`) REFERENCES `t_room` (`room_id`),
  CONSTRAINT `ref_user` FOREIGN KEY (`user_id`) REFERENCES `t_user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_timetable
-- ----------------------------
INSERT INTO `t_timetable` VALUES ('4', '1', '1', '12:00:00', '1', '1', '1');
INSERT INTO `t_timetable` VALUES ('5', '1', '1', '22:22:00', '1', '1', '1');
INSERT INTO `t_timetable` VALUES ('6', '1', '1', '06:22:00', '1', '1', '1');
INSERT INTO `t_timetable` VALUES ('7', '1', '1', '22:22:00', '1', '1', '1');
INSERT INTO `t_timetable` VALUES ('9', '1', '1', '22:22:00', '1', '1', '1');
INSERT INTO `t_timetable` VALUES ('10', '1', '1', '22:22:00', '1', '1', '1');
INSERT INTO `t_timetable` VALUES ('11', '1', '1', '22:22:00', '1', '1', '1');
INSERT INTO `t_timetable` VALUES ('12', '1', '1', '22:22:00', '1', '1', '1');
INSERT INTO `t_timetable` VALUES ('13', '1', '1', '22:22:00', '1', '1', '1');
INSERT INTO `t_timetable` VALUES ('14', '1', '1', '22:22:00', '1', '1', '1');
INSERT INTO `t_timetable` VALUES ('15', '1', '1', '22:22:00', '1', '1', '1');
INSERT INTO `t_timetable` VALUES ('16', '1', '1', '22:22:00', '1', '1', '1');
INSERT INTO `t_timetable` VALUES ('17', '1', '1', '22:00:22', '1', '1', '1');
INSERT INTO `t_timetable` VALUES ('18', '1', '1', '22:22:00', '1', '1', '1');
INSERT INTO `t_timetable` VALUES ('19', '1', '1', '22:22:00', '1', '1', '1');
INSERT INTO `t_timetable` VALUES ('20', '1', '1', '22:22:00', '1', '1', '1');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_phone` varchar(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_sex` char(1) DEFAULT NULL,
  `user_wxId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', '12345678912', '测试', '男', null);
